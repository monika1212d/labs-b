package pk.labs.LabB;

public class LabDescriptor {

    // region P1
    public static String displayImplClassName = "...";
    public static String controlPanelImplClassName = "...";

    public static String mainComponentSpecClassName = "...";
    public static String mainComponentImplClassName = "...";
    public static String mainComponentBeanName = "...";
    // endregion

    // region P2
    public static String mainComponentMethodName = "...";
    public static Object[] mainComponentMethodExampleParams = new Object[] { };
    // endregion

    // region P3
    public static String loggerAspectBeanName = "...";
    // endregion
}
